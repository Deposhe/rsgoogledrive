//
//  GTLDriveFile+RSFolder.m
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "GTLDriveFile+RSFolder.h"

@implementation GTLDriveFile (RSFolder)

- (RSFolder *)folder
{
    RSFolder * folder = [[RSFolder alloc] init];
    
    folder.folderId = self.identifier;
    folder.folderName = self.name;
    folder.iconName = @"GoogleDrive.png";
    folder.source = RSFolderSourceGoogleDrive;
    folder.isSync = [[self.appProperties.JSON valueForKey:@"isSyncronized"] boolValue];
    return folder;
}
@end