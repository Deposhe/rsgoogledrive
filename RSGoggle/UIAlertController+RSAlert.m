//
//  UIAlertController+RSAlert.m
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "UIAlertController+RSAlert.h"

@implementation UIAlertController (RSAlert)

+ (void)sc_showErrorMessageWithTitle: (NSString *) title
                             message: (NSString *) message
                          controller: (UIViewController *) controller
                           okHandler: (nullable AcceptHandler) handler;
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title
                                                                              message:message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:handler];
    
    [alertController addAction:defaultAction];
    
    [controller presentViewController:alertController
                             animated:YES
                           completion:nil];
}

+ (void) sc_showFieldAlertWithAlertTitle: (NSString *) title
                                 message: (NSString *) message
                             acceptTitle: (NSString *) acceptTitle
                            declineTitle: (NSString *) declineTitle
                              controller: (UIViewController *) controller
                           acceptHandler: (AlertSimpleHandler) acceptHandler
                          declineHandler: (AlertSimpleHandler) declineHandler
                     alertTextFieldBlock: (AlertTextFieldConfiguration) alertTextFieldBlock
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * acceptAction = [UIAlertAction actionWithTitle:acceptTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (acceptHandler)
            acceptHandler(alertController);
    }];
    
    UIAlertAction * declineAction = [UIAlertAction actionWithTitle:declineTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (declineHandler)
            declineHandler(alertController);
    }];
    
    [alertController addTextFieldWithConfigurationHandler:alertTextFieldBlock];
    [alertController addAction:acceptAction];
    [alertController addAction:declineAction];
    [controller presentViewController:alertController animated:YES completion:nil];
}

@end