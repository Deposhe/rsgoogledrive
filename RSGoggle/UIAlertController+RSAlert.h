//
//  UIAlertController+RSAlert.h
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void (^AcceptHandler)(UIAlertAction * _Nonnull action);
typedef void (^AlertSimpleHandler)(UIAlertController * alertController);
typedef void (^AlertTextFieldConfiguration)(UITextField *textField);

@interface UIAlertController (RSAlert)

+ (void)sc_showErrorMessageWithTitle: (NSString *) title
                             message: (NSString *) message
                          controller: (UIViewController *) controller
                           okHandler: (nullable AcceptHandler) handler;

+ (void) sc_showFieldAlertWithAlertTitle: (NSString *) title
                                 message: (NSString *) message
                             acceptTitle: (NSString *) acceptTitle
                            declineTitle: (NSString *) declineTitle
                              controller: (UIViewController *) controller
                           acceptHandler: (AlertSimpleHandler) acceptHandler
                          declineHandler: (AlertSimpleHandler) declineHandler
                     alertTextFieldBlock: (AlertTextFieldConfiguration) alertTextFieldBlock;

@end

NS_ASSUME_NONNULL_END
