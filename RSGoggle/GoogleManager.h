//
//  GoogleManager.h
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTLDrive.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "RSFolder.h"

typedef void (^Completion)(NSError * error);
typedef void (^CompletionSearch)(BOOL founded, NSError * error);
typedef void (^CompletionResult)(NSArray * result, NSError * error);
typedef void (^CompletionPageResult)(BOOL lastPage, NSArray * result, NSError * error);

@class UIViewController;

@interface GoogleManager : NSObject

@property (nonatomic, strong) GTMOAuth2Authentication * credentials;

+ (BOOL) canBeAuthorized;
- (void) signInByViewController: (UIViewController *) viewController completion: (Completion) signInCompletion;

- (void) updateFolder:(RSFolder *)folder completion:(Completion)updateCompletion;

- (void) getListOfFoldersInRootFolder: (BOOL) rootFolderSearch result:(CompletionResult) result;
- (void) updateFolders:(NSArray <RSFolder *> *)folders completion:(Completion)updateCompletion;

@end
