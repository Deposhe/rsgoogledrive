//
//  GoogleManager.m
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "GoogleManager.h"
#import "RSUtils.h"
#import "AppDelegate.h"

static NSString *const kKeychainItemName = @"Snap2Cloud";
static NSString *const kClientId = @"904830618426-d8qq8ij785sssgo5coa77vkg1l6820g1.apps.googleusercontent.com";
static NSString *const kDefaultRootFolderName = @"Snap2CloudDemo";

@interface GoogleManager ()

@property (nonatomic, strong) NSString * rootS2CFolderId;
@property (nonatomic, copy) Completion authCompletion;
@property (nonatomic, strong) GTLServiceDrive * driveService;
@property (nonatomic, strong) GTMOAuth2ViewControllerTouch * authController;
@property (nonatomic, weak) UIViewController * loginViewController;
@property (nonatomic, copy) CompletionResult pageResultBlock;

@end

@implementation GoogleManager

- (void) signInByViewController: (UIViewController *) viewController completion: (Completion) signInCompletion
{
    self.authCompletion = signInCompletion;
    self.loginViewController = viewController;
    [viewController presentViewController:self.authController animated:YES completion:nil];
}

+ (BOOL) canBeAuthorized
{
    GTMOAuth2Authentication * credentials = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                                                  clientID:kClientId
                                                                                              clientSecret:nil];
    return [credentials canAuthorize];
}



#pragma mark - Private

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error
{
    [self.loginViewController dismissViewControllerAnimated:YES completion:nil];
    if (error == nil)
    {
        NSLog(@"%@", @"DRIVE - Authentication SUCCESS");
        [self isAuthorizedWithAuthentication:auth];
    }
    else
    {
        if (self.authCompletion)
            self.authCompletion (error);
        
        [UIAlertController sc_showErrorMessageWithTitle:@"Authorization Error"
                                                message:@"Please try again"
                                             controller:self.loginViewController
                                              okHandler:^(UIAlertAction * _Nonnull action) {
                                                  [self.loginViewController presentViewController:self.authController animated:YES completion:nil];
                                              }];
    }
}

- (void)isAuthorizedWithAuthentication:(GTMOAuth2Authentication *)auth
{
    [self driveService].authorizer = auth;
    
    [self searchRootFolder:^(BOOL founded, NSError *error) {
        NSLog(@"%@", @"DRIVE SEARCH ROOT Folder");
        if (!error && !founded)
        {
            NSLog(@"%@", @"DRIVE CREATE ROOT FOLDER");
            
            [self createFolder:nil root:YES completion:^(NSError *error) {
                if (!error)
                {
                    if (self.authCompletion)
                        self.authCompletion(nil);
                }
                else
                {
                    NSLog(@"%@", @"DRIVE - UNABLE TO CREATE FOLDER");
                    if (self.authCompletion)
                        self.authCompletion(error);
                }
            }];
        }
        else
        {
            NSLog(@"%@", @"DRIVE - DN NEED TO CREATE FOLDER");
            if (self.authCompletion)
                self.authCompletion(error ? error : nil);
        }
    }];
}

#pragma mark - Google Requests / Responses

- (void) getListOfFoldersInRootFolder: (BOOL) rootFolderSearch result:(CompletionResult) result
{
    GTLQueryDrive * query = [GTLQueryDrive queryForFilesList];
    NSString * queryRequest = [NSString string];
    if (rootFolderSearch)
    {
        queryRequest = [NSString stringWithFormat:@"'%@' in parents and trashed != true and mimeType='application/vnd.google-apps.folder'", self.rootS2CFolderId];
    }
    else
    {
        queryRequest = [NSString stringWithFormat:@"trashed != true and mimeType='application/vnd.google-apps.folder'"];
    }
    query.q = queryRequest;

    NSString * fieldsString = @"nextPageToken, files(id, name, mimeType, kind, trashed, parents, appProperties)";
    query.fields = fieldsString;
    query.pageSize = 450;
    [query setOrderBy:@"modifiedTime desc,name"];
    
    //NSLog(@"%@", query.fields);
    //self.driveService.shouldFetchNextPages = YES;
    
    GTLServiceTicket * googleTicket = [[self driveService] executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                                GTLDriveFileList *fileList,
                                                                NSError *error) {
        self.pageResultBlock = result;
        [self executePageQuery:query];
    }];
}

- (void) executePageQuery: (GTLQueryDrive *) query
{
    [[self driveService] executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLDriveFileList *fileList, NSError *error) {
        NSMutableArray * foldersArray = [NSMutableArray array];
        if (!error)
        {
            NSLog(@"COunt - %li", fileList.files.count);
            for (GTLDriveFile * driveFolder in fileList.files)
            {
                [foldersArray addObject: [driveFolder folder]];
            }
            self.pageResultBlock (foldersArray, nil);
            
            if (fileList.nextPageToken)
            {
                NSLog(@"counter - %li", ticket.pagesFetchedCounter);
                query.pageToken = fileList.nextPageToken;
                [self executePageQuery:query];
            }
        }
        else
        {
            self.pageResultBlock(nil, error);
        }
    }];
}


- (void)getFolderByFolderId:(NSString *)searchId completion: (CompletionResult) searchResult
{
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    query.q = [NSString stringWithFormat:@"'%@' in parents and trashed != true and mimeType='application/vnd.google-apps.folder'", self.rootS2CFolderId];
    [[self driveService] executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                                GTLDriveFileList *fileList,
                                                                NSError *error) {
        if (!error)
        {
            
            NSArray * resultArray = [NSArray array];
            for (GTLDriveFile * file in fileList.files)
            {
                if ([file.identifier isEqualToString:searchId])
                {
                    resultArray = @[[file folder]];
                    break;
                }
            }
            
            if (searchResult)
                searchResult(resultArray, nil);
        }
        else
        {
            if (searchResult)
                searchResult(nil, error);
        }
    }];
}

- (void) searchRootFolder: (CompletionSearch) completionSearch
{
    NSString *parentId = @"root";
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    query.q = [NSString stringWithFormat:@"'%@' in parents and trashed != true and '%@' in owners", parentId, self.credentials.userEmail];
    query.fields = @"files(id, name, mimeType, kind, trashed, parents, appProperties, properties)";
    [[self driveService] executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                                GTLDriveFileList *fileList,
                                                                NSError *error) {
        if (!error)
        {
            NSString * newRootId = [NSString string];
            for (GTLDriveFile * object in fileList.files)
            {
                if ([object.name isEqualToString:kDefaultRootFolderName])
                {
                    
                    newRootId = object.identifier;
                    break;
                }
            }
            
            if (newRootId.length > 0 && completionSearch)
            {
                self.rootS2CFolderId = newRootId;
                completionSearch(YES, nil);
            }
            else
            {
                completionSearch(NO, nil);
            }
        }
        else
        {
            if (completionSearch)
                completionSearch(NO, error);
            
            NSLog(@"DRIVE An error occurred: %@", error);
        }
    }];
}


- (void) createFolder: (NSString *) folderName root: (BOOL) isRoot completion: (Completion) createFolderCompletion
{
    GTLDriveFile *folder = [GTLDriveFile object];
    
    folder.mimeType = @"application/vnd.google-apps.folder";
    
    if (!isRoot)
    {
        folder.name = folderName;
        folder.parents = @[self.rootS2CFolderId];
    }
    else
    {
        folder.name = kDefaultRootFolderName;
    }
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesCreateWithObject:folder
                                                       uploadParameters:nil];
    
    [[self driveService] executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                                GTLDriveFile *updatedFile,
                                                                NSError *error) {
        if (!error)
        {
            NSLog(@"DRIVE Created folder SUCCESS");
            if(isRoot)
            {
                self.rootS2CFolderId = updatedFile.identifier;
            }
            
            if (createFolderCompletion)
                createFolderCompletion(nil);
        }
        else
        {
            NSLog(@"DRIVE Created folder FAILURE -> An error occurred: %@", error);
            if (createFolderCompletion)
                createFolderCompletion(error);
        }
    }];
}

- (void) updateFolder:(RSFolder *)folder completion:(Completion)updateCompletion
{
    GTLDriveFile * driveFolder = [GTLDriveFile object];
    driveFolder.name = folder.folderName;
    
    GTLDriveFileAppProperties * appProperties = [[GTLDriveFileAppProperties alloc] init];
    [appProperties setJSONValue:folder.isSync ? @"YES" : @"NO" forKey:@"isSyncronized"];
    driveFolder.appProperties = appProperties;
    
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesUpdateWithObject:driveFolder
                                                                 fileId:folder.folderId
                                                       uploadParameters:nil];
    
    [[self driveService] executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                                GTLDriveFile *updatedFile,
                                                                NSError *error) {
        if (!error)
        {
            if (updateCompletion)
                updateCompletion (nil);
        }
        else
        {
            if (updateCompletion)
                updateCompletion (error);
        }
    }];
}

- (void) updateFolders:(NSArray <RSFolder *> *)folders completion:(Completion)updateCompletion
{
    GTLBatchQuery * batchQuery = [[GTLBatchQuery alloc] init];

    for (RSFolder * folder in folders)
    {
        GTLDriveFile * driveFolder = [GTLDriveFile object];
        driveFolder.name = folder.folderName;
        
        GTLDriveFileAppProperties * appProperties = [[GTLDriveFileAppProperties alloc] init];
        [appProperties setJSONValue:folder.isSync ? @"YES" : @"NO" forKey:@"isSyncronized"];
        driveFolder.appProperties = appProperties;
        
        GTLQueryDrive *query = [GTLQueryDrive queryForFilesUpdateWithObject:driveFolder
                                                                     fileId:folder.folderId
                                                           uploadParameters:nil];
        [batchQuery addQuery:query];
    }
    
    [[self driveService] executeQuery:batchQuery completionHandler:^(GTLServiceTicket *ticket,
                                                                GTLDriveFile *updatedFile,
                                                                NSError *error) {
        if (!error)
        {
            if (updateCompletion)
                updateCompletion (nil);
        }
        else
        {
            if (updateCompletion)
                updateCompletion (error);
        }
    }];
}



#pragma mark - Properties

- (GTLServiceDrive *)driveService
{
    static GTLServiceDrive *service = nil;
    
    if (!service)
    {
        service = [[GTLServiceDrive alloc] init];
        //service.shouldFetchNextPages = YES;
        service.retryEnabled = YES;
    }
    if (!service.authorizer)
    {
        service.authorizer = self.credentials;
    }
    
    return service;
}

- (GTMOAuth2Authentication *)credentials
{
    _credentials = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                         clientID:kClientId
                                                                     clientSecret:nil];
    return _credentials;
}



- (GTMOAuth2ViewControllerTouch *)authController
{
    SEL finishedSelector = @selector(viewController:finishedWithAuth:error:);
    GTMOAuth2ViewControllerTouch *authViewController =
    [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeDrive
                                               clientID:kClientId
                                           clientSecret:nil
                                       keychainItemName:kKeychainItemName
                                               delegate:self
                                       finishedSelector:finishedSelector];
    
    authViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    return authViewController;
}

#pragma mark - User Defaults Using

- (NSString *)rootS2CFolderId
{
    return [RSDelegate rootFolderId];
}

- (void)setRootS2CFolderId:(NSString *)rootS2CFolderId
{
    NSLog(@"%@", @"DRIVE - SETROOTFOLDER");
    [RSDelegate setRootFolderId:rootS2CFolderId];
}




@end
