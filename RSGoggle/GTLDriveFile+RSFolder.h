//
//  GTLDriveFile+RSFolder.h
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "GTLDriveFile.h"
#import "RSFolder.h"

@interface GTLDriveFile (RSFolder)

- (RSFolder*) folder;

@end
