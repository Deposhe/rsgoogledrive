//
//  ViewController.m
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "ViewController.h"
#import "GoogleManager.h"
#import "RSFolder.h"
#import "RSCustomCell.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *signInBUtton;

@property (nonatomic, strong) GoogleManager * manager;
@property (nonatomic, strong) NSMutableArray * dataSource;

@property (nonatomic, weak) IBOutlet UITableView * tblFolders;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GoogleManager * manager = [[GoogleManager alloc] init];
    self.manager = manager;
}

- (void) viewDidAppear:(BOOL)animated
{
    self.dataSource = [NSMutableArray array];
    [super viewDidAppear:animated];
    if ([self.manager.credentials canAuthorize])
    {
        [self requestForFiles];
        return;
    };
    [self.manager signInByViewController:self completion:^(NSError *error) {
        if (!error)
        {
            NSLog(@"%@", @"DRIVE IN CONTROLLER");
            [self requestForFiles];
        }
        else
        {
            NSLog(@"%@", @"DRIVE IN CONTROLLER eRROR");
        }
        
    }];
}

- (void) requestForFiles
{
    [self.manager getListOfFoldersInRootFolder:NO result:^(NSArray *result, NSError *error) {
        [self.dataSource addObjectsFromArray:result];
        [self.tblFolders reloadData];
    }];
}


#pragma mark - Actions
- (IBAction)didPressUpdateButton:(UIButton *)sender
{
    RSFolder * folder = self.dataSource[1];
    folder.isSync = NO;
    [self.manager updateFolder:folder completion:^(NSError *error) {
        NSLog(@"%@", @"drive UPdate Completed");
    }];
}

- (IBAction)didPressCellSwithcer:(UISwitch *)sender
{
    RSFolder * folder = self.dataSource[sender.tag];
    folder.isSync = sender.isOn;
    
}

- (IBAction)didPressDoneButton:(id)sender
{
    NSLog(@"%@", @"Update");
    /*for (RSFolder * folder in self.dataSource)
    {
        [self.manager updateFolder:folder completion:^(NSError *error) {
            NSLog(@"%@ - updated", folder.folderName);
        }];
        
       
    }*/
    [self.manager updateFolders:self.dataSource completion:^(NSError *error) {
        NSLog(@"- updated");
    }];
}

- (IBAction)didPressRefreshButton:(id)sender
{
    [self requestForFiles];
}


#pragma mark - ITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"Cell";
    RSCustomCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        cell = [[RSCustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    RSFolder * folder = self.dataSource[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%li - %@", indexPath.row, folder.folderName];
    cell.imageView.image = [UIImage imageNamed:folder.iconName];
    cell.switcher.tag = indexPath.row;
    cell.switcher.on = folder.isSync;
    [cell.switcher addTarget:self action:@selector(didPressCellSwithcer:) forControlEvents:UIControlEventValueChanged];
    return cell;
}

#pragma mark - UITableViewDelegate
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}



@end
