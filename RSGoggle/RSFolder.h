//
//  RSFolder.h
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    RSFolderSourceUndefinded,
    RSFolderSourceGoogleDrive,
    RSFolderSourceICloud,
    RSFolderSourceDropBox
}   RSFolderSource;

@interface RSFolder : NSObject

@property (nonatomic, strong) NSString * folderName;
@property (nonatomic, strong) NSString * folderId;
@property (nonatomic, strong) NSString * iconName;
@property (nonatomic) BOOL isSync;
@property (nonatomic) RSFolderSource source;

@end
