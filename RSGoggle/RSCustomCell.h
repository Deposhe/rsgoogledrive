//
//  RSCustomCell.h
//  RSGoggle
//
//  Created by Сергей on 22.10.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UISwitch *switcher;

@end
